///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     100
#define inf    10000000
#define MD     1000000007
#define eps    1e-9
///===============================///

int dx[]={0,0,1,-1};
int dy[]={1,-1,0,0};

int dp[MX][MX];
int vis[MX+2][MX+2];
int main() {
    freopen("input_0.txt","r",stdin);
    freopen("Output_0.txt","w",stdout);
    int tc,n,x,y,l,r,i,j,k;
    cin>>tc;
    for(int cs=1; cs<=tc; cs++) {
        cin>>n;
        for(int i=1;i<=n;i++){
            for(int j=1;j<=n;j++){
                cin>>x;
                if(x==1)dp[i][j]=1;
                else dp[i][j]=0;
                vis[i][j]=0;
            }
        }
        cin>>x>>y;
        int cnt=1;
        queue<int>Q;
        Q.push(x);
        Q.push(y);
        vis[x][y]=1;
        while( !Q.empty() ){
            int x=Q.front(); Q.pop();
            int y=Q.front(); Q.pop();
            for(int i=0;i<4;i++){
                int nx=x+dx[i];
                int ny=y+dy[i];
                if( nx>=1 && nx<=n && ny>=1 && ny<=n && dp[nx][ny]==1 && !vis[nx][ny]){
                    cnt++;
                    vis[ nx ][ ny ]=1;
                    Q.push(nx);
                    Q.push(ny);
                }
            }
        }
        cout<<cnt<<endl;
    }
    return 0;
}
