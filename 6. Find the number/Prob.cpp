///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     1000012
#define inf    10000000000
#define MD     1000000007
#define eps    1e-9
///===============================///

ll n,x,y,i,sum,a,b,c,d;
map<int,int>Mp;
string s;
int main() {
    freopen("input_0.txt","r",stdin);
    freopen("Output_0.txt","w",stdout);
    int tc;
    cin>>tc;
    for(int cs=1; cs<=tc; cs++) {
        Mp.clear();
        cin>>n>>x;
        for(int i=1;i<=n;i++){
            cin>>y;
            Mp[ y ]++;
        }
        if( Mp[x] )cout<< "YES\n";
        else cout<< "NO\n";
    }
    return 0;
}
