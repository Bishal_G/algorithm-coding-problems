///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     200012
#define inf    10000000000
#define MD     1000000007
#define eps    1e-9
///===============================///

int n,pl;
int ar[MX+2],primes[MX+2];

bool IsPrime(int n) {
    int sq=sqrt( n );
    if( n<=1 ) return false;
    if( n==2 ) return true;
    if( n%2==0 ) return false;
    for(int i=2; i<=sq; i++) {
        if(n%i==0) return false;
    }
    return true;
}

void genPrimes(int n) {
    pl=0;
    for(int i=1; i<=n; i++) {
        if( IsPrime(i) ){
            primes[ pl++ ]=i;
        }
    }
}

int main() {
    // freopen("input.txt","r",stdin);
    srand(632312);
    freopen("input_0.txt","w",stdout);
    genPrimes(100000);
    //cout<<pl<<endl;
   // for(int i=0;i<pl;i++)cout<<primes[i]<< " ";

    int tc;
    tc=20;
    cout<<tc<<endl;
    for(int cs=1; cs<=tc; cs++) {
        n=rand()%1000+1;
        if( cs==tc )n=1000;

        ar[0]=rand()%10000;
        for(int i=1; i<n; i++) {
            ar[i]=rand()%100000;
            if( rand()%5==0 ){
                ar[i]=primes[ rand()%pl ];
            }
        }

        cout<<n<<endl;
        cout<<ar[0];
        for(int i=1; i<n; i++) {
            cout<< " "<<ar[i];
        }
        cout<<endl;

    }
    return 0;
}
