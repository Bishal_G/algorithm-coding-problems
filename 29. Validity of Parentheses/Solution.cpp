///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;

///  Digit     0123456789012345678 ///
#define MX     100005
#define inf    2000000010
#define MD     1000000007
#define eps    1e-9
///===============================///


char s[MX+2];

bool Ok(int n) {
    if(n&1) return 0;
    stack<char>St;
    for(int i=0; i<n; i++) {
        if( s[i]=='{' || s[i]=='[' || s[i]=='(' ) {
            St.push( s[i] );
        }
        if( s[i]=='}' || s[i]==']' || s[i]==')' ) {
            if( St.empty() ) return 0;
            char tp=St.top();
            if( tp=='[' && s[i]!=']' ) return 0;
            if( tp=='(' && s[i]!=')' ) return 0;
            if( tp=='{' && s[i]!='}' ) return 0;
            St.pop();
        }
    }
    return (St.empty());
}

int main() {
    int n,i,j,k,x,y,tc,cs=1;
    S(tc);
    while(tc--) {
        scanf("%s",s);
        n=strlen(s);
        if( Ok(n) ) printf("YES\n");
        else printf("NO\n");
    }
    return 0;
}
