///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     1000012
#define inf    10000000000
#define MD     1000000007
#define eps    1e-9
///===============================///

ll n,x;
ll ar[MX+2];
int main() {
    // freopen("input.txt","r",stdin);
    srand(345343);
    freopen("input_0.txt","w",stdout);
    int tc;
    tc=20;
    cout<<tc<<endl;
    for(int cs=1; cs<=tc; cs++) {
         n=rand()%999+2;

         ar[0]=rand()%100;
         for(int i=1;i<n;i++){
            ar[i]=rand()%10000;
            if(rand()%2)ar[i]*=-1;
         }

        // sort( ar,ar+n );


         cout<<n<<endl;
         cout<<ar[0];
         for(int i=1;i<n;i++){
            cout<< " "<<ar[i];
         }
         cout<<endl;

    }
    return 0;
}
