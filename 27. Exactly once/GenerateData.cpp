///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     200002
#define inf    10000000000
#define MD     1000000007
#define eps    1e-9
///===============================///

vector<int>v;
int main() {
    // freopen("input.txt","r",stdin);
    srand(65026);
    freopen("input_0.txt","w",stdout);
    int tc;
    tc=20;
    cout<<tc<<endl;
    for(int i=1; i<=tc; i++) {
        int n=rand()%20000+1;
        int m=(2*n);
        v.clear();
        for(int j=1;j<=n;j++){
            v.push_back(j);
            v.push_back(j);
        }
        random_shuffle( v.begin(),v.end() );
        vector<int>tmp;
        int sz=v.size();
        int sk=rand()%sz;
        for(int j=0;j<v.size();j++){
            if(sk!=j)tmp.push_back( v[j] );
        }
        cout<<tmp.size()<<endl;
        cout<<tmp[0];
        for(int j=1;j<tmp.size();j++){
            cout<< " "<<tmp[j];
        }
        cout<<endl;
    }
    return 0;
}
