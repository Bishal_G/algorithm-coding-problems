///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     1000012
#define inf    10000000000
#define MD     1000000007
#define eps    1e-9
///===============================///

int ar[MX+2];
int main() {
    freopen("input_0.txt","r",stdin);
    freopen("Output_0.txt","w",stdout);
    int tc,n,x,i,j,k;
    cin>>tc;
    for(int cs=1; cs<=tc; cs++) {
        cin>>n;
        for(int i=1; i<=n; i++) {
            cin>>ar[i];
        }
        sort(ar+1,ar+n+1);
        if( n&1 ) {
            int mid=ar[(n+1)/2];
            cout<<mid<<endl;
        } else {
            int x=ar[ n/2 ];
            int y=ar[ n/2+1 ];
            cout<<(x+y)/2;
            if( (abs(y-x))&1 )cout<< ".5";
            cout<<endl;
        }
    }
    return 0;
}
