#include <fstream>
#include <iostream>
#include <string>
using namespace std;

int main( ) {
    // freopen("A-small-attempt0.in", "r", stdin);
    // freopen("Output_A.txt", "w", stdout);
    ifstream in("OutputSaanvi1.txt"); //open two ins and one out
    ifstream in2("OutputSaanvi2.txt");
    ofstream end("MatchSaanvi.txt");
    while ((!in.eof()) && (!in2.eof())) { //continue you get til the end of both

        string line,line2;
        getline(in,line); //get lines
        getline(in2,line2);
        if(line!=line2) { //are the strings equal? i.e. on the same line and same?
            end<<line << "\n";//if so write it to out file
        }
    }

    in.close();
    in2.close();   //close them
    end.close();

    return 0;  //open your out file and enjoy
}


