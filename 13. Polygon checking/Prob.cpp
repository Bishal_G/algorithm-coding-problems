///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     1000012
#define inf    10000000000
#define MD     1000000007
#define eps    1e-9
///===============================///

pii p[10];

int Dis(int i,int j){
    int xx=(p[i].X-p[j].X)*(p[i].X-p[j].X);
    int yy=(p[i].Y-p[j].Y)*(p[i].Y-p[j].Y);
    return xx+yy;
}

int main() {
    freopen("input_0.txt","r",stdin);
    freopen("Output_0.txt","w",stdout);
    int tc;
    cin>>tc;
    for(int cs=1; cs<=tc; cs++) {
        fr(i,1,4)cin>>p[i].X>>p[i].Y;
        int a=Dis(1,2);
        int b=Dis(2,3);
        int c=Dis(3,4);
        int d=Dis(4,1);
        if( a==c && b==d && a==b ) printf("SQUARE\n");
        else if( a==c && b==d )printf("RECTANGLE\n");
        else printf("POLYGON\n");
    }
    return 0;
}
