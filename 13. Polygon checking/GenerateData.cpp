///==================================================///
///                HELLO WORLD !!                    ///
///                  IT'S ME                         ///
///               BISHAL GAUTAM                      ///
///         [ bsal.gautam16@gmail.com ]              ///
///==================================================///
#include<bits/stdc++.h>
#define X first
#define Y second
#define mpp make_pair
#define nl printf("\n")
#define SZ(x) (int)(x.size())
#define pb(x) push_back(x)
#define pii pair<int,int>
#define pll pair<ll,ll>
///---------------------
#define S(a) scanf("%d",&a)
#define P(a) printf("%d",a)
#define SL(a) scanf("%lld",&a)
#define S2(a,b) scanf("%d%d",&a,&b)
#define SL2(a,b) scanf("%lld%lld",&a,&b)
///------------------------------------
#define all(v) v.begin(),v.end()
#define CLR(a) memset(a,0,sizeof(a))
#define SET(a) memset(a,-1,sizeof(a))
#define fr(i,a,n) for(int i=a;i<=n;i++)
using namespace std;
typedef long long ll;
///==========CONSTANTS=============///
///  Digit     0123456789012345678 ///
#define MX     1000012
#define inf    10000000000
#define MD     1000000007
#define eps    1e-9
///===============================///

ll n;
int main() {
    // freopen("input.txt","r",stdin);
    freopen("input_0.txt","w",stdout);
    int tc;
    tc=6;
    cout<<tc<<endl;
    ///Square
    cout<<0<< " "<<0<<endl;
    cout<<0<< " "<<1<<endl;
    cout<<1<< " "<<1<<endl;
    cout<<1<< " "<<0<<endl;

    ///Rectangle
    cout<<0<< " "<<0<<endl;
    cout<<0<< " "<<5<<endl;
    cout<<10<< " "<<5<<endl;
    cout<<10<< " "<<0<<endl;

    ///Polygons
    cout<<0<< " "<<0<<endl;
    cout<<0<< " "<<8<<endl;
    cout<<2<< " "<<15<<endl;
    cout<<4<< " "<<2<<endl;

     ///Square
    cout<<1<< " "<<1<<endl;
    cout<<1<< " "<<50<<endl;
    cout<<50<< " "<<50<<endl;
    cout<<50<< " "<<1<<endl;

    ///Rectangle
    cout<<1<< " "<<0<<endl;
    cout<<1<< " "<<15<<endl;
    cout<<100<< " "<<15<<endl;
    cout<<100<< " "<<0<<endl;

    ///Polygons
    cout<<1<< " "<<1<<endl;
    cout<<2<< " "<<48<<endl;
    cout<<12<< " "<<55<<endl;
    cout<<44<< " "<<2<<endl;


    return 0;
}
